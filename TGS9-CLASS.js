console.log('====================');
console.log('Tugas 9 Class');
console.log('NO 1');
console.log('====================');

class Animal {
    constructor(name, legs, cold_blooded) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 

class Ape {
    constructor(name,legs, sound){
        this.name = name 
        this.legs = 2
        this.sound = "Auooo"
    }
    yell(){
        console.log(`${this.sound}`);
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() 

class Frog {
    constructor(name, legs, sound){
        this.name = name 
        this.legs = 4
        this.sound = "hop hop" 
    }
    jump(){
        console.log(`${this.sound}`);
    }
}
var kodok = new Frog("buduk")
kodok.jump() 

console.log('====================');
console.log('NO 2');
console.log('====================');
 
  class Clock {
    constructor({ template }) {
        this.template = template;
      }
    
      render() {
        let date = new Date();
    
        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        let output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
      }
    
      stop() {
        clearInterval(this.timer);
      }
    
      start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
      }
    }
 
var clock = new Clock({template: 'h:m:s'});
clock.start();
